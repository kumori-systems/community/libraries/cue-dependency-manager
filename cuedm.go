/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cuedm

import (
	"fmt"

	"gitlab.com/kumori-systems/community/libraries/cue-dependency-manager/pkg/helper"
)

const (
	maxDepth = 100
)

// Resolve function resolves dependencies detected in src directory.
// All dependencies are stored (flatted) in src/cue.mod/pkg directory
// Returns a context keeping track of all resolution operations
func Resolve(src string) (ctx *Context, err error) {

	moduleCueFile := src + "/cue.mod/module.cue"

	// Src must be a cue module
	if !helper.FileExists(moduleCueFile) {
		err = fmt.Errorf("Context is not a CUE module")
		return
	}

	// Dst must be empty
	dst := src + "/cue.mod/pkg"
	if helper.DirExists(dst) {
		_, err = helper.EmptyDir(dst)
		if err != nil {
			return
		}
	}

	// Read cue module file
	cuemodule, err := NewCUEModuleFromFile(moduleCueFile)
	if err != nil {
		return
	}

	// Initializes context with dependencies of src module
	ctx = &Context{
		Src:        src,
		Dst:        dst,
		CUEModules: map[string]*CUEModule{},
	}
	ctx.AddCUEModule(cuemodule)

	// Iteration while there are still dependencies to be resolved (until maxdepth)
	for i := 0; i <= maxDepth; i++ {
		if i == maxDepth {
			err = fmt.Errorf(
				"cuedm.Resolved has reached the maximum number of iterations(%d)",
				maxDepth,
			)
			return
		}
		err = resolveIteration(ctx)
		if err != nil {
			return
		}
		if ctx.Resolved() {
			break
		}
	}

	return
}

func resolveIteration(ctx *Context) (err error) {
	for _, module := range ctx.CUEModules {
		if !module.Resolved {
			err = module.Resolve(ctx)
			if err != nil {
				return
			}
		}
	}
	return
}
