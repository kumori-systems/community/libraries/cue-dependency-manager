/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cuedm

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCUEModuleFromFile(t *testing.T) {

	moduleCueFile := "./test_data/readcuemodule/module.cue"
	dep_version := "v1.2.3"
	dep_srcPath := "v1.2.3"
	dep_tag := "mycommit"
	dep_dstPath := "mydependency.com/things/v1.2.3"
	dep_token := "mytoken"
	mycredentials := Credentials{
		Token:    &dep_token,
		Type:     "token",
		Username: "myuser",
	}
	expectedResult := CUEModule{
		Name: "mycompany.com/mymodule/v1.0.0",
		Dependencies: map[string]*Dependency{
			"mydependency.com/things": &Dependency{
				Name:          "mydependency.com/things",
				Version:       &dep_version,
				VersionedName: "mydependency.com/things/v1.2.3",
				Repository:    "https://gitlab.com/myrepository/things",
				Credentials:   &mycredentials,
				SrcPath:       &dep_srcPath,
				Tag:           &dep_tag,
				DstPath:       &dep_dstPath,
				Resolved:      false,
			},
			"otherdependency.com/thongs": &Dependency{
				Name:          "otherdependency.com/thongs",
				VersionedName: "otherdependency.com/thongs",
				Repository:    "https://gitlab.com/otherrepository/thongs",
				Resolved:      false,
			},
		},
		Resolved: false,
	}

	cuemodule, err := NewCUEModuleFromFile(moduleCueFile)
	if err != nil {
		fmt.Println("ERROR: ", err.Error())
		t.Fatal(err)
	}
	assert.Equal(t, *cuemodule, expectedResult)
}

func TestNewCUEModuleFromEmptyFile(t *testing.T) {

	moduleCueFile := "./test_data/readcuemodule/emptymodule.cue"
	expectedResult := CUEModule{
		Name:     "mycompany.com/mymodule",
		Resolved: false,
	}
	cuemodule, err := NewCUEModuleFromFile(moduleCueFile)
	if err != nil {
		fmt.Println("ERROR: ", err.Error())
		t.Fatal(err)
	}
	assert.Equal(t, *cuemodule, expectedResult)
}

func TestCompleteTestv1(t *testing.T) {
	src := "./test_data/completetestv1"

	// Remove from old executions

	ctx, err := Resolve(src)
	if err != nil {
		t.Fatal(err)
	}

	ctx_mymodule, ok := ctx.CUEModules["mymodule/0.0.1"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_mymodule.Name, "mymodule/0.0.1")
	assert.Equal(t, len(ctx_mymodule.Dependencies), 2)
	assert.Equal(t, ctx_mymodule.Resolved, true)

	ctx_c, ok := ctx.CUEModules["kumori-test.com/c"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_c.Name, "kumori-test.com/c")
	assert.Equal(t, len(ctx_c.Dependencies), 2)
	assert.Equal(t, ctx_c.Resolved, true)

	ctx_de, ok := ctx.CUEModules["kumori-test.com/de/v1.0.0"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_de.Name, "kumori-test.com/de/v1.0.0")
	assert.Equal(t, len(ctx_de.Dependencies), 0)
	assert.Equal(t, ctx_de.Resolved, true)

	ctx_f, ok := ctx.CUEModules["kumori-test.com/f"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_f.Name, "kumori-test.com/f")
	assert.Equal(t, len(ctx_f.Dependencies), 0)
	assert.Equal(t, ctx_f.Resolved, true)
}

func TestCompleteTestv2(t *testing.T) {
	src := "./test_data/completetestv2"

	// Remove from old executions

	ctx, err := Resolve(src)
	if err != nil {
		t.Fatal(err)
	}

	ctx_mymodule, ok := ctx.CUEModules["mymodule/0.0.1"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_mymodule.Name, "mymodule/0.0.1")
	assert.Equal(t, len(ctx_mymodule.Dependencies), 2)
	assert.Equal(t, ctx_mymodule.Resolved, true)

	ctx_c, ok := ctx.CUEModules["kumori-test.com/c"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_c.Name, "kumori-test.com/c")
	assert.Equal(t, len(ctx_c.Dependencies), 2)
	assert.Equal(t, ctx_c.Resolved, true)

	ctx_dev1, ok := ctx.CUEModules["kumori-test.com/de/v1.0.0"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_dev1.Name, "kumori-test.com/de/v1.0.0")
	assert.Equal(t, len(ctx_dev1.Dependencies), 0)
	assert.Equal(t, ctx_dev1.Resolved, true)

	ctx_dev2, ok := ctx.CUEModules["kumori-test.com/de/v2.0.0"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_dev2.Name, "kumori-test.com/de/v2.0.0")
	assert.Equal(t, len(ctx_dev2.Dependencies), 0)
	assert.Equal(t, ctx_dev2.Resolved, true)

	ctx_f, ok := ctx.CUEModules["kumori-test.com/f"]
	assert.Equal(t, ok, true)
	assert.Equal(t, ctx_f.Name, "kumori-test.com/f")
	assert.Equal(t, len(ctx_f.Dependencies), 0)
	assert.Equal(t, ctx_f.Resolved, true)
}

func TestCompleteTestv2NoCredentials(t *testing.T) {
	src := "./test_data/completetestv2_nocredentials"
	_, err := Resolve(src)
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "authentication required")
}

func TestNotACueMod(t *testing.T) {
	src := "./test_data/not_a_cuemodule"
	_, err := Resolve(src)
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "Context is not a CUE module")
}
