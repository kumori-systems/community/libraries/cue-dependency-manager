/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cuedm

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	dst   = "./test_data/clonetest"
	url   = "https://gitlab.com/kumori/kv3/unit-tests-fixtures/cue-dependency-manager/testing.git"
	user  = "justfortest"
	token = "hB4FrDeWiyDE_P9uPwEU"
)

func TestGitDownloaderVersionWithReference(t *testing.T) {
	err := os.RemoveAll(dst)
	if err != nil {
		t.Fatal(err)
	}
	version := "v2.0.0"
	tag := ""
	src := ""
	err = Clone(url, user, token, version, tag, src, dst)
	if err != nil {
		t.Fatal(err)
	}
	content, err := ioutil.ReadFile(dst + "/README.md")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, string(content), "Esto es v2.0.0")
}

func TestGitDownloaderVersionWithDirectory(t *testing.T) {
	err := os.RemoveAll(dst)
	if err != nil {
		t.Fatal(err)
	}
	version := "v3.0.0"
	tag := ""
	src := ""
	err = Clone(url, user, token, version, tag, src, dst)
	if err != nil {
		t.Fatal(err)
	}
	content, err := ioutil.ReadFile(dst + "/README.md")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, string(content), "Esto es v3.0.0")
}

func TestGitDownloaderVersionWithOldReference(t *testing.T) {
	err := os.RemoveAll(dst)
	if err != nil {
		t.Fatal(err)
	}
	version := "v1.0.0"
	tag := ""
	src := ""
	err = Clone(url, user, token, version, tag, src, dst)
	if err != nil {
		t.Fatal(err)
	}
	content, err := ioutil.ReadFile(dst + "/README.md")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, string(content), "Esto es v1.0.0")
}

func TestGitDownloaderVersionWithReferenceInBranch(t *testing.T) {
	err := os.RemoveAll(dst)
	if err != nil {
		t.Fatal(err)
	}
	version := "v1.0.2"
	tag := ""
	src := ""
	err = Clone(url, user, token, version, tag, src, dst)
	if err != nil {
		t.Fatal(err)
	}
	content, err := ioutil.ReadFile(dst + "/README.md")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, string(content), "Esto es v1.0.2")
}

func TestGitDownloaderTag(t *testing.T) {
	err := os.RemoveAll(dst)
	if err != nil {
		t.Fatal(err)
	}
	version := "v3.0.0" // Version is ignored
	tag := "v2.0.0"
	src := ""
	err = Clone(url, user, token, version, tag, src, dst)
	if err != nil {
		t.Fatal(err)
	}
	content, err := ioutil.ReadFile(dst + "/README.md")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, string(content), "Esto es v2.0.0")
}

func TestGitDownloaderDirectoryFromMaster(t *testing.T) {
	err := os.RemoveAll(dst)
	if err != nil {
		t.Fatal(err)
	}
	version := ""
	tag := ""
	src := "v3.0.0"
	err = Clone(url, user, token, version, tag, src, dst)
	if err != nil {
		t.Fatal(err)
	}
	content, err := ioutil.ReadFile(dst + "/README.md")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, string(content), "Esto es v3.0.0")
}

func TestGitDownloaderInvalidUrl(t *testing.T) {
	version := "v3.0.0"
	tag := ""
	src := ""
	err := Clone(url+"_noexists", user, token, version, tag, src, dst)
	assert.Equal(
		t,
		err.Error(),
		"repository not found",
	)
}

func TestGitDownloaderInvalidToken(t *testing.T) {
	version := ""
	tag := "v2.0.0"
	src := ""
	err := Clone(url, user, token+"_invalid", version, tag, src, dst)
	assert.Equal(
		t,
		err.Error(),
		"authentication required",
	)
}

func TestGitDownloaderNoCredentials(t *testing.T) {
	version := ""
	tag := "v2.0.0"
	src := ""
	err := Clone(url, "", "", version, tag, src, dst)
	assert.Equal(
		t,
		err.Error(),
		"authentication required",
	)
}

func TestGitDownloaderInvalidTag(t *testing.T) {
	version := ""
	tag := "invalid_tag"
	src := ""
	err := Clone(url, user, token, version, tag, src, dst)
	assert.Equal(
		t,
		err.Error(),
		"No matching reference found in the repository",
	)
}
