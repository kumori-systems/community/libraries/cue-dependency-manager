/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cue2json

import (
	"fmt"
	"os/exec"
	"path/filepath"

	"gitlab.com/kumori-systems/community/libraries/cue-dependency-manager/pkg/helper"
)

// U is just an alias to the type allowing manipulate json without go-structs
type U = map[string]interface{}

// File permission
const filePerm = 0664
const dirPerm = 0777

// ExportOne executes the "cue export" command for only one cue file.
func ExportOne(srcFile string, dstFile string) (err error) {

	srcDir := filepath.Dir(srcFile)
	srcFileName := filepath.Base(srcFile)
	currentWorkDir, err := helper.ChangeWorkDir(srcDir)
	if err != nil {
		return
	}
	defer func() {
		_, err2 := helper.ChangeWorkDir(currentWorkDir)
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	// Compile cue file
	cmd := exec.Command("cue", "export", srcFileName)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		err = fmt.Errorf("%s (%s)", err.Error(), string(output))
		return
	}

	err = helper.CreateFile(dstFile, filePerm, output)
	if err != nil {
		return
	}

	return
}
