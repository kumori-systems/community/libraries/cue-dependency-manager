/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package helper

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
)

// FileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists checks if a dir exists and is directory before we
// try using it to prevent further errors.
func DirExists(dir string) bool {
	info, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// ChangeWorkDir changes the workdir, and returns the current workdir
func ChangeWorkDir(newWorkDir string) (oldWorkDir string, err error) {
	oldWorkDir, err = os.Getwd()
	if err != nil {
		return
	}
	err = os.Chdir(newWorkDir)
	return
}

// CreateFile creates a file (ensuring that the directory is created, if it does
// not exist) with the provided content
func CreateFile(
	fileFullName string, filePerm os.FileMode, content []byte,
) (
	err error,
) {
	filePath := filepath.Dir(fileFullName)
	err = os.MkdirAll(filePath, os.ModeDir|0777)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(fileFullName, content, filePerm)
	if err != nil {
		return
	}
	return
}

// CreateFiles work like CreateFile, but for several files
func CreateFiles(
	fileFullNames []string, filePerm os.FileMode, contents [][]byte,
) (
	err error,
) {
	for k, fileFullName := range fileFullNames {
		filePath := filepath.Dir(fileFullName)
		content := contents[k]
		err = os.MkdirAll(filePath, os.ModeDir|0777)
		if err != nil {
			return
		}
		err = ioutil.WriteFile(fileFullName, content, filePerm)
		if err != nil {
			return
		}
	}
	return
}

// RenameIfExists renames a directory if exists, ensuring that the destination
// exists.
func RenameIfExists(src string, dst string) (err error) {
	if DirExists(src) {
		err = os.MkdirAll(filepath.Dir(dst), os.ModeDir|0777)
		if err != nil {
			return
		}
		err = os.Rename(src, dst)
		if err != nil {
			return
		}
	}
	return
}

// EmptyDir removes all contents from a given folder but not the folder itself
func EmptyDir(dir string) (int, error) {

	// Checks if the dst directoty is or not empty
	file, err := os.Open(dir)
	if err != nil {
		fmt.Printf("\n\nError: %s %s\n\n", dir, err.Error())
		return 0, err
	}
	files, err := file.Readdir(0)
	if err != nil {
		fmt.Printf("\n\nError: %s %s\n\n", dir, err.Error())
		return 0, err
	}

	// If the folder is already empty, nothing to do.
	if len(files) == 0 {
		return 0, nil
	}

	// Remove all content in the folder
	removed := 0
	for _, fileInfo := range files {
		filePath := path.Join(dir, fileInfo.Name())
		if fileInfo.IsDir() {
			err := os.RemoveAll(filePath)
			if err != nil {
				fmt.Printf("\n\nError: %s %s\n\n", filePath, err.Error())
				return removed, err
			}
			removed++
			continue
		}
		err := os.Remove(filePath)
		if err != nil {
			fmt.Printf("\n\nError: %s %s\n\n", filePath, err.Error())
			return removed, err
		}
		removed++
	}

	return removed, nil
}
