/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cuedm

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/kumori-systems/community/libraries/cue-dependency-manager/pkg/cue2json"
	"gitlab.com/kumori-systems/community/libraries/cue-dependency-manager/pkg/helper"
)

// Credentials ...
type Credentials struct {
	Type     string  `json:"type"` // "password" or "token"
	Username string  `json:"username"`
	Password *string `json:"password,omitempty"`
	Token    *string `json:"token,omitempty"`
}

// GetPasswordOrToken return token or password, depending on type of credentials
func (c *Credentials) GetPasswordOrToken() string {
	if c.Type == "token" {
		return *c.Token
	}
	return *c.Password
}

// Dependency ...
type Dependency struct {
	Name          string
	Version       *string      `json:"version,omitempty"`
	VersionedName string       // VersionedName = name/version
	Repository    string       `json:"repository"`
	Credentials   *Credentials `json:"credentials,omitempty"`
	SrcPath       *string      `json:"srcpath,omitempty"`
	Tag           *string      `json:"tag,omitempty"`
	DstPath       *string      `json:"dstpath,omitempty"`
	Resolved      bool
}

// CUEModule ...
type CUEModule struct {
	Name         string                 `json:"module"`                 // Its a versioner name (not mandatory)
	Dependencies map[string]*Dependency `json:"dependencies,omitempty"` // Key is dependency name
	Resolved     bool
}

// NewCUEModuleFromFile reads a module.cue file, and extracts dependencies info.
//
// For example:
//     module: "mycompany.com/mymodule/v1.0.0" // This line is ignored by cue-dependencies-manager
//
//     // Comments are allowed (this is a CUE file, not a JSON)
//
//     credentials: {
//     	mycredentials: {
//     		token: "mytoken",
//     		type: "token",
//     		username: "myuser"
//     	}
//     }
//
//     dependencies: {
//     	 "mydependency.com/things": { // Name of dependency
//     	 	version: "v1.2.3", // Optional. Default: no version
//     	 	repository: "https://gitlab.com/myrepository/things"
//     	 	credentials: "mycredentials" // Optional. Default: no credentials required
//     	 	srcpath: "v1.2.3" // Optional.
//     	 										// Default="v1.2.3", if directory "v1.2.3" exists within the repo
//     	 										// Default="/", if directory "v1.2.3" not exists within the repo
//     	 	tag: "mycommit"   // Optional.
//     	 									  // Default="master".
//     	 										// Can be setted with the commit/tag of an specific version
//     	 	dstpath: "mydependency.com/things/v1.2.3" // Optional. Default value is <name>/<version>
//     	 },
//     	 "otherdependency.com/thongs": {
//     	 	repository: "https://gitlab.com/otherrepository/thongs"
//     	 }
//     }
func NewCUEModuleFromFile(filePath string) (cuemodule *CUEModule, err error) {
	cuemodule = &CUEModule{}

	tmpFile, err := filepath.Abs("./.tmpmodulecue.json")
	if err != nil {
		return
	}
	defer func() {
		err2 := os.RemoveAll(tmpFile)
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	err = cue2json.ExportOne(filePath, tmpFile)
	if err != nil {
		return
	}

	content, err := ioutil.ReadFile(tmpFile)
	if err != nil {
		return
	}

	err = json.Unmarshal(content, &cuemodule)
	if err != nil {
		return
	}

	for k, v := range cuemodule.Dependencies {
		v.Resolved = false
		v.Name = k
		if v.Version == nil || *v.Version == "" {
			v.VersionedName = k
		} else {
			v.VersionedName = k + "/" + *v.Version
		}
	}
	cuemodule.Resolved = false

	return
}

// Resolve function resolves the module dependencies, detecting the new
// generated dependencies (that are added to the context)
func (c *CUEModule) Resolve(ctx *Context) (err error) {
	for _, dep := range c.Dependencies {
		if ctx.DependencyResolved(dep.VersionedName) {
			// This dependency has been already resolved in other module
			dep.Resolved = true
		} else {
			err = c.resolveDependency(dep, ctx)
			if err != nil {
				return
			}
		}
	}
	c.Resolved = true
	return
}

// resolveDependency function resolves a dependency, detecting the new
// generated dependencies (that are added to the context)
func (c *CUEModule) resolveDependency(dep *Dependency, ctx *Context) (err error) {
	cloneDst, srcPath, version, repository, tag, username, tokenOrPassword,
		err := c.getCloneParameters(dep, ctx)
	if err != nil {
		return
	}
	if !helper.DirExists(cloneDst) {
		err = Clone(
			repository, username, tokenOrPassword, version, tag, srcPath, cloneDst,
		)
		if err != nil {
			return
		}

		// New dependency is added to the context, to be resolved
		var dependencyCueModule *CUEModule
		dependencyCueModule, err = NewCUEModuleFromFile(cloneDst + "/cue.mod/module.cue")
		if err != nil {
			return
		}
		ctx.AddCUEModule(dependencyCueModule)
	}
	return
}

func (c *CUEModule) getCloneParameters(
	dep *Dependency, ctx *Context,
) (
	dstPath, srcPath, version, repository, tag, username, tokenOrPassword string,
	err error,
) {

	dstPath = ""
	if dep.DstPath != nil && *dep.DstPath != "" {
		dstPath = ctx.Dst + "/" + *dep.DstPath
	} else {
		dstPath = ctx.Dst + "/" + dep.VersionedName
	}

	srcPath = ""
	if dep.SrcPath != nil && *dep.SrcPath != "" {
		srcPath = *dep.SrcPath
	}

	version = ""
	if dep.Version != nil && *dep.Version != "" {
		version = *dep.Version
	}

	repository = dep.Repository

	tag = ""
	if dep.Tag != nil && *dep.Tag != "" {
		tag = *dep.Tag
	}

	username = ""
	tokenOrPassword = ""
	if dep.Credentials != nil {
		username = dep.Credentials.Username
		tokenOrPassword = dep.Credentials.GetPasswordOrToken()
	}

	return
}
