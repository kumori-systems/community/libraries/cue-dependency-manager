module gitlab.com/kumori-systems/community/libraries/cue-dependency-manager

go 1.13

// ORIGINAL
// require (
// 	github.com/stretchr/testify v1.5.0
//  github.com/go-git/go-git/v5 v5.3.0
// )

require (
	github.com/stretchr/testify v1.5.0
	github.com/go-git/go-git/v5 v5.3.0
)
