module: "mycompany.com/mymodule/v1.0.0" // This line is ignored by cue-dependencies-manager

// Comments are allowed (this is a CUE file, not a JSON)

creds = {
	mycredentials: {
		token: "mytoken",
		type: "token",
		username: "myuser"
	}
}

dependencies: {
	"mydependency.com/things": { // Name of dependency
		version: "v1.2.3", // Optional. Default: no version
		repository: "https://gitlab.com/myrepository/things"
		credentials: creds.mycredentials // Optional. Default: no credentials required"
		srcpath: "v1.2.3" // Optional.
											// Default="v1.2.3", if directory "v1.2.3" exists within the repo
											// Default="/", if directory "v1.2.3" not exists within the repo
		tag: "mycommit" // Optional.
			   						// Default="master".
										// Can be setted with the commit/tag of an specific version
		dstpath: "mydependency.com/things/v1.2.3" // Optional. Default value is <name>/<version>
	},
	"otherdependency.com/thongs": {
		repository: "https://gitlab.com/otherrepository/thongs"
	}
}
