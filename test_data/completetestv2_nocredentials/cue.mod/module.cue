module: "mymodule/0.0.1"

// Will fail, due to no-credentials

dependencies: {
  "kumori-test.com/c": {
    repository: "https://gitlab.com/kumori/kv3/unit-tests-fixtures/cue-dependency-manager/cue-c"
    //credentials: creds.cred_c
  },
  "kumori-test.com/de": {
    repository: "https://gitlab.com/kumori/kv3/unit-tests-fixtures/cue-dependency-manager/cue-d-e"
    version: "v1.0.0"
    //credentials: creds.cred_de
  }
}
