package B

import c "kumori-test.com/c"
import d "kumori-test.com/de:D"

B :: {
  Bvalue: string
  ...
}

myB :: c.C & d.D & {
  Bvalue: "BBB",
  Cvalue: "CCC"
  Evalue: "EEE"
  Fvalue: "FFF"
  Dvalue: "DDD"
}

myB