package B

import d "kumori-test.com/de/v2.0.0:D"
import e "kumori-test.com/de/v2.0.0:E"

B :: {
  Bvalue: string
  ...
}

myB :: B & d.D & e.E & {
  Bvalue: "BBB",
  Dvalue: "DDD"
  Evalue: 333
}

myB