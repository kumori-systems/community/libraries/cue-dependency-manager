package a

import c "kumori-test.com/c"

A :: {
  Avalue: string
  ...
}

myA :: A & c.C & {
  Avalue: "AAA",
  Cvalue: "CCC"
  Evalue: "EEE"
  Fvalue: "FFF"
}

myA