module: "mymodule/0.0.1"

creds = {
  cred_c: {
    type: "token",
    username: "just-for-test",
    token: "F24JTTP3MP7n3vfeNrRz"
  }
  cred_de: {
    type: "token",
    username: "just-for-test",
    token: "kjof2FMdjfr7AX_d5siF"
  }
}

dependencies: {
  "kumori-test.com/c": {
    repository: "https://gitlab.com/kumori/kv3/unit-tests-fixtures/cue-dependency-manager/cue-c"
    credentials: creds.cred_c
  },
  "kumori-test.com/de": {
    repository: "https://gitlab.com/kumori/kv3/unit-tests-fixtures/cue-dependency-manager/cue-d-e"
    version: "v1.0.0"
    credentials: creds.cred_de
  }
}
