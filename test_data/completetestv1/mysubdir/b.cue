package B

import c "kumori-test.com/c"
import d "kumori-test.com/de/v1.0.0:D"

B :: {
  Bvalue: string
  ...
}

myB :: c.C & d.D & {
  Bvalue: "BBB",
  Cvalue: "CCC"
  Evalue: "EEE"
  Fvalue: "FFF"
  Dvalue: "DDD"
}

myB