# CUE Dependency Manager

GO package for CUE dependency manager

## Structure and restrictions of modules and packages in CUE

- There is a file `cue.mod/module.cue`, containing the name of the module. +
  For example: `module: "mycompany.com/mymodule"`.
- Any additional element can be added to the cue.mod/module.cue file: CUE only uses the `<module: module_name>` line.
- A module is composed of any number of packages, organized in a free files-and-directories structure. +
For example, we can have a "bli" package in `/bla/ble/bli.cue` file. +
Every file begins with the name of the package it belongs to.
- From any package you can refer to another package of the module, simply using the "import" sentence composed by: +
`import [alias] <module_name>/<relative position of package within module>[:<package name>]` +
The name of the package can be omitted if it matches the name of the file (quite common)
- If you have dependencies on an external module, it must be copied (with the same rules) into the `/pkg` directory
- From any package you can refer to a package of a dependency, using the"import" sentence composed by: +
`import [alias] <dependency_module_name>/<relative position of package within module>[:<package name>]` +
That is: just like a package from the module itself.
- The `/pkg` directory can be "flattened".


For example:

```
├── bla
│   └── ble
│       ├── ble.cue // begins with "package ble"
│       └── bli.cue // begins with "package bli"
│
├── myfile.cue // Contains:
│              // import b1 "mycompany.com/mymodule/bla/ble"
│              // import b2 "mycompany.com/mymodule/bla/ble:bli"
│              // import t1 "mydependency.com/things/thing1"
│              // import t2 "mydependency.com/things/thing2"
│
└── cue.mod
    ├── module.cue // Contains: module: "mycompany.com/mymodule"
    └── pkg
        └── mydependency.com
            └── things
                ├── cue.mod
                │   └── module.cue // Contains: module: "mydependency.com/things"
                ├── thing1
                │   └── thing1.cue // Begins with "package thing1"
                └── thing2
                    └── thing2.cue // Begins with "package thing2"
```

## Versioning

CUE is not explicitly concerned with versioning.

In the file `cue.mod/module.cue`, we recommend (but it isnt mandatory) use a vesioned name. +
For example: `module: "mycompany.com/mymodule/v1.2.3"`.

That versioned name must be used (of course) in import sentences:

We can implement it this way:

```
├── bla
│   └── ble
│       ├── ble.cue // begins with "package ble"
│       └── bli.cue // begins with "package bli"
│
├── myfile.cue // Contains:
│              // import b1 "mycompany.com/mymodule/bla/ble"
│              // import b2 "mycompany.com/mymodule/bla/ble:bli"
│              // import t1 "mydependency.com/things/v1.2.3/thing1"
│              // import t2 "mydependency.com/things/v1.2.3/thing2"
│
└── cue.mod
    ├── module.cue // Contains: module: "mycompany.com/mymodule"
    └── pkg
        └── mydependency.com
            └── things
                └── v1.2.3
                    ├── cue.mod
                    │   └── module.cue // Contains: module: "mydependency.com/things/v1.2.3"
                    ├── thing1
                    │   └── thing1.cue // Begins with "package thing1"
                    └── thing2
                        └── thing2.cue // Begins with "package thing2"		  
```

## Dependency declaration

In the `cue.mod/module.cue` file itself, we can declare all our dependencies.

```
module: "mycompany.com/mymodule/v1.0.0"

// Comments are allowed (this is a CUE file, not a JSON)

credentials = {
  mycredentials: {
    type: "token", // "token" or "password"
    username: "myuser"
    token: "mytoken", // or: password: "mypassword"
  }
}

dependencies: {
  "mydependency.com/things": { // Name of dependency
    version: "v1.2.3", // Optional. Default: no version (but recommended!)
                       // Version can be a tag or a directory
    repository: "https://gitlab.com/myrepository/things"
    credentials: credentials.mycredentials // Optional. Default: no credentials required
    srcpath: "v1.2.3" // Optional.
                      // Default="v1.2.3", if directory "v1.2.3" exists within the repo
                      // Default="/", if directory "v1.2.3" not exists within the repo
    tag: "mytag" // Optional.
                 // Default="master".
                 // Can be setted with the tag of an specific version
    dstpath: "mydependency.com/things/v1.2.3" // Optional. Default value is <name>/<version>
  },
  "otherdependency.com/thongs": {
    repository: "https://gitlab.com/otherrepository/thongs"
  }
}
```

### About version, srcpath and tag fields

Parameters srcpath, tag, version (tag type), version (dir type) may overlap, if all of them have value.
They should be interpreted as follows:

```
├── tag<>""
│   ├─ srcpath<>"" ==> use that tag and src directory. ver_dir/ver_ref is ignored
│   └─ srcpath!="" ==> use that tag and root directory. ver_dir/ver_ref is ignored
│
└── tag==""
    ├─ ver_ref<>""
    │  ├─ srcpath<>"" ==> use that ver_ref commit and src directorycommit
    │  └─ srcpath=="" ==> use that ver_ref
    └─ ver_ref==""
       ├─ srcpath<>"" ==> use master commmit and src directory.
       └─ srcpath==""
          ├─ ver_dir<>"" ==> use master commit and ver_dir directory
          └─ ver_dir=="" ==> use master commit
```

## Example of use

```
package main

import (
	"fmt"
	cuedm "gitlab.com/kumori-systems/community/libraries/cue-dependency-manager"
)

func main() {

  // Resolve dependencies
  src := "./test_data/completetestv2"
	ctx, err := cuedm.Resolve(src)
	if err != nil {
		fmt.Println("ERROR", err.Error())
		return
	}

  // For debug purposes... print results
	fmt.Println("---------------------------------------------------------------")
	for _, module := range ctx.CUEModules {
		fmt.Println("Module:", module.Name)
		fmt.Println("  Resolved:", module.Resolved)
		fmt.Println("  Dependencies: (", len(module.Dependencies), ")")
		for _, dep := range module.Dependencies {
			fmt.Println("    Name: ", dep.VersionedName)
			fmt.Println("    Repository: ", dep.Repository)
		}
		fmt.Println("")
	}
	fmt.Println("---------------------------------------------------------------")
}
```

## License

Copyright 2022 Kumori systems S.L.

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and limitations under the Licence.
