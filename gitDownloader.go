/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cuedm

import (
	"fmt"
	"os"

	"gitlab.com/kumori-systems/community/libraries/cue-dependency-manager/pkg/helper"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
)

// Clone clones a git repository in a given folder
//
// - Dst folder is created if not exist.
//   BE CAREFUL: dst folder is removed before git-clone.
// - Src folder is optional, and means that we only are interested in that directory
//   from git repository.
// - tag is optional, and means that we only are interested in that tag. Tag
//   parameter take precedence over version parameter.
// - Version is optional, and is its interpretation depends on the case:
//   - If exists a tag==version, then that tag (its reference) is used
//   - If not exists a tag==version, but exists a directory==version, then that
//     directory is used
//
// Parameters src, tag, version (tag type), version (dir type) may overlap,
// if all of them have value.
// They should be interpreted as follows:
//
// ├── tag<>""
// │   ├─ src<>"" ==> use that tag and src directory. ver_dir/ver_ref is ignored
// │   └─ src!="" ==> use that tag and root directory. ver_dir/ver_ref is ignored
// │
// └── tag==""
//     ├─ ver_ref<>""
//     │  ├─ src<>"" ==> use that ver_ref commit
//     │  └─ src=="" ==> use that ver_ref commit and src directory
//     └─ ver_ref==""
//        ├─ src<>"" ==> use master commmit and src directory.
//        └─ src==""
//           ├─ ver_dir<>"" ==> use master commit and ver_dir directory
//           └─ ver_dir=="" ==> use master commit
//
func Clone(
	url string,
	userName string, password string,
	version string,
	tag string,
	src string,
	dst string,
) (
	err error,
) {

	//
	// Prepare destination directory, and create a temporary directory
	//
	tmpDst, err := prepareDst(dst)
	defer os.RemoveAll(tmpDst)

	//
	// Clone repo in temporary directory
	//
	repo, err := plainClone(url, userName, password, tmpDst)
	if err != nil {
		return
	}

	//
	// What commit must be used? Tag, version or master?
	// Review all repo references, checking if any match tag or version
	//
	var useTag = false
	var useVersionRef = false
	var useMaster = false
	var masterReference, finalReference plumbing.Hash
	finalName := ""
	iter, err := repo.References()
	err = iter.ForEach(func(ref *plumbing.Reference) error {
		name, commit, err := getNameAndCommit(ref, repo)
		if err != nil {
			return err
		}
		if tag != "" && (tag == name || tag == commit.String()) {
			useTag = true
			finalReference = commit
			finalName = name
		}
		if tag == "" && version != "" && version == name {
			useVersionRef = true
			finalReference = commit
			finalName = name
		}
		if name == "master" {
			masterReference = commit
		}
		return nil
	})
	if err != nil {
		return
	}
	if tag != "" && !useTag {
		err = fmt.Errorf("No matching reference found in the repository")
		return
	}
	if !useTag && !useVersionRef {
		useMaster = true
		finalReference = masterReference
		finalName = "master"
	}
	if !useTag && !useVersionRef && !useMaster {
		err = fmt.Errorf("No matching reference found in the repository")
		return
	}

	//
	// What src directory must be used? Src, version or root?
	//
	finalSrc := ""
	if useTag {
		if src != "" {
			finalSrc = src
		}
	} else if useVersionRef {
		if src != "" {
			finalSrc = src
		}
	} else {
		if src != "" {
			finalSrc = src
		} else if version != "" {
			finalSrc = version
		}
	}

	//
	// Checkout the right commit
	//
	worktree, err := repo.Worktree()
	if err != nil {
		return
	}
	options := git.CheckoutOptions{}
	options.Hash = finalReference
	if err = worktree.Checkout(&options); err != nil {
		err = fmt.Errorf(
			"Error checking out reference '%s' from repository '%s': %s",
			finalName, url, err.Error(),
		)
		return
	}

	//
	// Src directory must exists
	//
	if finalSrc != "" {
		if !helper.DirExists(tmpDst + "/" + finalSrc) {
			err = fmt.Errorf("Required directory %s not found", finalSrc)
			return
		}
	}

	//
	// Move content from dst/tmp to dst, only taking into account the source
	// directory if specified.
	//
	if finalSrc != "" {
		err = helper.RenameIfExists(tmpDst+"/"+finalSrc, dst)
	} else {
		err = helper.RenameIfExists(tmpDst, dst)
	}
	if err != nil {
		return
	}

	return
}

func prepareDst(dst string) (tmpDst string, err error) {

	// dst folder is removed before git-clone.
	err = os.RemoveAll(dst)
	if err != nil {
		return
	}

	// A tmp directory is used during cloning
	tmpDst = dst + "_tmp"
	err = os.MkdirAll(tmpDst, os.ModeDir|0777)
	if err != nil {
		return
	}

	// Removes content of the destination folder since it isn't empty
	_, err = helper.EmptyDir(tmpDst)
	if err != nil {
		err = fmt.Errorf("Error removing content from folder '%s': %s", dst, err.Error())
		return
	}

	return
}

func plainClone(
	url string, userName string, password string, dst string,
) (
	repo *git.Repository, err error,
) {
	options := &git.CloneOptions{
		URL:               url,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	}
	if userName != "" {
		auth := http.BasicAuth{
			Username: userName,
			Password: password,
		}
		options.Auth = &auth
	}
	repo, err = git.PlainClone(dst, false, options)
	return
}

func getNameAndCommit(
	ref *plumbing.Reference, repo *git.Repository,
) (
	name string, commit plumbing.Hash, err error,
) {

	name = ref.Name().Short()
	obj, err := repo.TagObject(ref.Hash()) // Annotated tags!
	switch err {
	case nil:
		// Its an annotated tag -> get its real commit
		commit = obj.Target
	case plumbing.ErrObjectNotFound:
		err = nil
		commit = ref.Hash()
	}
	return
}
