/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package cuedm

import "fmt"

// Context keeps track the status of a resolution operation (which dependency
// are resolved and which are not yet)
type Context struct {
	Src        string                // Src = main cue module
	Dst        string                // Dst = src/cue.mod/pkg
	CUEModules map[string]*CUEModule // key map is the versioned name of module
}

// Resolved checks if all CUEModules in the context has been resolved
func (ctx *Context) Resolved() bool {
	for _, v := range ctx.CUEModules {
		if !v.Resolved {
			return false
		}
	}
	return true
}

// DependencyResolved checks if a dependency has been resolved previously
func (ctx *Context) DependencyResolved(versionedName string) bool {
	for _, cm := range ctx.CUEModules {
		for _, d := range cm.Dependencies {
			if d.VersionedName == versionedName && d.Resolved {
				return true
			}
		}
	}
	return false
}

// AddCUEModule adds a new CUE module to the context
func (ctx *Context) AddCUEModule(cuemodule *CUEModule) {
	ctx.CUEModules[cuemodule.Name] = cuemodule
}

// Print is used for debug purposes
func (ctx *Context) Print() {
	for k, v := range ctx.CUEModules {
		fmt.Println(k, v.Resolved)
	}
}
